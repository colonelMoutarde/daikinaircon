<?php
/**
 * Copyright (C) 2021 luc Sanchez.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
declare(strict_types=1);

namespace Daikin;

use Daikin\Exceptions\DaikinException;
use Daikin\Interfaces\PushInterface;

final class SendAirconControlInfo implements PushInterface
{
    private Connect $connect;

    public function __construct(Connect $connect)
    {
        $this->connect = $connect;
    }

    /**
     * @param array<string, string> $parameters
     * @throws DaikinException
     */
    public function push(array $parameters): string
    {
        return $this->connect->call('/aircon/set_control_info', true, $parameters);
    }
}
