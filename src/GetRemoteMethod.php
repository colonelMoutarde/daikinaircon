<?php
/**
 * Copyright (C) 2021 luc Sanchez.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
declare(strict_types=1);

namespace Daikin;

use Daikin\Exceptions\DaikinException;
use Daikin\Interfaces\PresentationInterface;

final class GetRemoteMethod extends FormatValuesAbstract implements PresentationInterface
{
    private Connect $connect;

    public function __construct(Connect $connect)
    {
        $this->connect = $connect;
    }

    /**
     * @return array<int|string, array<mixed>|string>
     * @throws DaikinException
     */
    public function format(): array
    {
        return $this->toArray(explode(',', $this->connect->call('/common/get_remote_method', true)));
    }
}
