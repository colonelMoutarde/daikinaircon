<?php
/**
 * Copyright (C) 2021 luc Sanchez.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

declare(strict_types=1);

namespace Daikin;

use Daikin\Exceptions\DaikinException;
use Symfony\Component\HttpClient\HttpClient;
final class Connect
{
    private const GET = 'GET';
    private const POST = 'POST';
    private string $domaine;

    /**
     * @throws DaikinException
     */
    public function __construct(string $domaine)
    {
        if (false === filter_var($domaine, FILTER_VALIDATE_IP)) {
            throw new DaikinException('Invalid ip');
        }
        $this->domaine = $domaine;
    }

    public function getPreparedeDomain(?bool $secured): string
    {
        $http = '';
        if (true === $secured) {
            $http = 's';
        }

        return "http{$http}://{$this->domaine}";
    }

    /**
     * @param ?array<string, string> $parameters
     */
    public function call(string $uri, bool $isPost, ?array $parameters = null): string
    {
        $client = HttpClient::create();
        $method = self::GET;
        if (true === $isPost) {
            $method = self::POST;
        }

        $response = $client->request($method, $this->getPreparedeDomain(false).$uri, ['query' => $parameters]);

        if (200 !== $response->getStatusCode()) {
            throw new DaikinException('The device return code --> '.$response->getStatusCode(), $response->getStatusCode());
        }

        return $response->getContent();

    }
}
