<?php

declare(strict_types=1);

namespace Daikin\Enums;

enum EquipmentStatus: int
{
    case cool = 1;
    case overcool = 2;
    case heat = 3;
    case fan = 4;
    case idle = 5;
}
