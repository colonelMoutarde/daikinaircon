# For using the Daikin Lib:

Basic usage

Need php 8.1

```php
<?php
require_once('vendor/autoload.php');

use Daikin\SendAirconControlInfo;
use Daikin\Connect;
use Daikin\Enums\Mode;
use Daikin\GetAirconModelInfo;
use Daikin\GetBasicInformation;
use Daikin\GetRemoteMethod;
use Daikin\GetAirconSensorInfo;
use Daikin\Enums\Power;
use Daikin\Enums\FanRate;

$connect = new Connect('192.168.1.117'); //the IP of the AIRCON

var_dump((new GetBasicInformation($connect))->format());
var_dump((new GetRemoteMethod($connect))->format());
var_dump((new GetAirconModelInfo($connect))->format());
var_dump((new GetAirconSensorInfo($connect))->format());
var_dump((new SendAirconControlInfo($connect))->push([
     'pow' => Power::OFF->value,
     'mode' => Mode::COLD->value,
     'stemp' => 20,
     'shum' => 0,
     'f_rate' => FanRate::SILENCE->value,
     'f_dir' => FanRate::LVL_1->value,
]));
```
