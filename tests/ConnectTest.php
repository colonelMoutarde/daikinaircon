<?php

declare(strict_types=1);

namespace Daikin\Tests;

use Daikin\Connect;
use Daikin\Exceptions\DaikinException;
use PHPUnit\Framework\TestCase;

final class ConnectTest extends TestCase
{
    private const MOCK_DOMAIN = '192.168.1.1'; // Remplacer par une adresse IP valide pour vos tests

    public function testConstructThrowsExceptionOnInvalidDomain(): void
    {
        $this->expectException(DaikinException::class);
        new Connect('invalid_domain');
    }

    public function testGetPreparedeDomain(): void
    {
        $connect = new Connect(self::MOCK_DOMAIN);
        $this->assertEquals('http://'.self::MOCK_DOMAIN, $connect->getPreparedeDomain(false));
        $this->assertEquals('https://'.self::MOCK_DOMAIN, $connect->getPreparedeDomain(true));
    }
}
